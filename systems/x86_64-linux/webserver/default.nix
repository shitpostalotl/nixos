{ config, inputs, ... }:
{
	imports = [ ./hardware-configuration.nix ];

	nix.settings.experimental-features = [ "nix-command" "flakes" ];

	boot.loader = {
		systemd-boot.enable = true;
		efi.canTouchEfiVariables = true;
		efi.efiSysMountPoint = "/boot/efi";
	};

	networking = {
		hostName = "webserver";
		dhcpcd.enable = true;
		firewall.allowedUDPPorts = [ 80 443 3000 8096 4447 7070 ];
		firewall.allowedTCPPorts = config.networking.firewall.allowedUDPPorts;
	};

	time.timeZone = "America/Los_Angeles";

	users = {
		users.axolotl = {
			isNormalUser = true;
			extraGroups = [ "wheel" ];
			hashedPassword = "$y$j9T$530Hm3lEfnVmDBGSu2aHa.$i2j0vbEpx7tPYazyKLrch1UbKbVZni3jm.wVExvYId1";
			openssh.authorizedKeys.keys = [ ''ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINqn8vPLuLEjDZvvWc+hI2oBoAapLzHZQIQcYqYlf1Tm "shitpostalotl@unix.dog"'' ''ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHhD6uo7CsJlNACFEcavvf35UEjrDgM/TJCqlCQnWaMQqccuLbm/5MJnp9hgqDkCkaSfWsaY1VdyE1UsI96P77dUL6w1yWkyafeDVu4+mWyxXwbk+YkCcjMi45OpuBssg5jiJzbLe93rMoQFR2MINODWSZAj08LKSk0XypnxhF9MkNzMgYgrXQiK4V5SowR+7TS+ONeEaPX0b1NViTcKo+s+epJra2T+vGVBi2G1Vpxt9Jwv0RkHBRYkY+Kt7mC7CSm5U6LH01q638a+jhFZP1TJgLumWXi3UeDb8uA9BHvWwDlaFfHWz/PMeweh1Cpkj8H0Fh+SWjICa8MLrtAGyZ'' ];
		};
		mutableUsers = false;
	};
	
	programs.git.enable = true;

	systemd.services."bandcampify" = {
		script = inputs.bandcampify.packages.x86_64-linux.default+"/bin/bandcampify /nas/Music";
		serviceConfig.User = "axolotl";
		startAt = "daily"; # systemctl status bandcampify.timer
	};
	services = {
		openssh = {
			enable = true;
			ports = [ 23 ];
			settings.PasswordAuthentication = false;
			settings.KbdInteractiveAuthentication = false;
		};
		iceshrimp = { # journalctl -ef -b 0 -u iceshrimp
			enable = true;
			createDb = true;
			configureNginx.enable = true;
			dbPasswordFile = "/iceshrimp/dbPasswordFile";
			secretConfig = "/iceshrimp/secretConfig.yml";
			settings = {
				db.host = "/run/postgresql";
				url = "https://axfedi.derg.rest";
				reservedUsernames = [ "shitpostalotl" ];
				searchEngine = "https://search.constellatory.net/search?q=";
			};
		};
		i2pd = {
			enable = true;
			address = "0.0.0.0";
			proto = {
				http.enable = true;
				httpProxy.enable = false;
				socksProxy.enable = true;
			};
		};
		nfssh = {
			enable = true;
			servers."10.0.0.85" = {
				server.user = "axolotl";
				local.user = "axolotl";
				mounts."/nas" = "/volume1/media";
			};
		};
		jellyfin = { enable = true; openFirewall = true; };
	};

	security = {
		acme = { acceptTerms = true; defaults.email = "shitpostalotl@unix.dog"; };
		sudo.enable = false;
		doas = {
			enable = true;
			wheelNeedsPassword = false;
		};
	};

	system.stateVersion = "22.05";
}
