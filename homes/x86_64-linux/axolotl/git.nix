{ pkgs, host, ... }: {
	programs = {
		git = {
			enable = true;
			package = pkgs.gitMinimal;
			extraConfig = {
				init.defaultBranch = "main";
				merge.conflictstyle = "diff3";
				pull.rebase = false;
				gpg.format = "ssh";
				gpg.ssh.allowedSignersFile = "~/Documents/.stuff/ssh_allowed_signers";
				log.showSignature = true;
				commit.gpgsign = true;
				tag.gpgsign = true;
				safe.directory = "/etc/nixos";
				diff.tool = "kitty";
				difftool.prompt = false;
				difftool.trustExitCode = true;
				"difftool \"kitty\"".cmd = "kitten diff $LOCAL $REMOTE";
			};
			includes = [
				{ condition = "hasconfig:remote.*.url:git@github.com:*/**"; contents.user = (import secrets/schoolgit.nix) host; }
				{ condition = "hasconfig:remote.*.url:git@codeberg.org:*/**"; contents.user = {
					name = "shitpostalotl";
					email = "shitpostalotl@unix.dog"; 
					signingkey = {
						"laptop" = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINqn8vPLuLEjDZvvWc+hI2oBoAapLzHZQIQcYqYlf1Tm shitpostalotl@unix.dog";
						"floortop" = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHhD6uo7CsJlNACFEcavvf35UEjrDgM/TJCqlCQnWaMQqccuLbm/5MJnp9hgqDkCkaSfWsaY1VdyE1UsI96P77dUL6w1yWkyafeDVu4+mWyxXwbk+YkCcjMi45OpuBssg5jiJzbLe93rMoQFR2MINODWSZAj08LKSk0XypnxhF9MkNzMgYgrXQiK4V5SowR+7TS+ONeEaPX0b1NViTcKo+s+epJra2T+vGVBi2G1Vpxt9Jwv0RkHBRYkY+Kt7mC7CSm5U6LH01q638a+jhFZP1TJgLumWXi3UeDb8uA9BHvWwDlaFfHWz/PMeweh1Cpkj8H0Fh+SWjICa8MLrtAGyZ";
					}.${host};
				}; }
			];
			aliases = {
				tree = "log --graph --all --color=always --abbrev-commit --decorate --date=relative --pretty=medium";
				diff = "git difftool --no-symlinks --dir-diff";
				retag = "!f() { git push origin :refs/tags/$1; git tag -fa $1; git push origin --tags; }; f";
			};
			attributes = [ "*.pdf diff=pdf" ];
			lfs.enable = true;
		};
		lazygit = {
			enable = true;
			package = with pkgs; writeShellScriptBin "lg" (lazygit+"/bin/lazygit");
			settings = {
				gui = {
					showRandomTip = false;
					nerdFontsVersion = "3";
					showDivergenceFromBaseBranch = "arrowAndNumber";
					border = "single";
					filterMode = "fuzzy";
					spinner = { frames = [ "Ooo" "oOo" "ooO" "oOo" ]; rate = 100; };
				};
				git.log.showWholeGraph = true;
				git.mainBranches = [ "master" "main" "pages" ];
				disableStartupPopups = true;
				notARepository = "skip";
				update.method = "never";
			};
		};
		ssh = {
			enable = true;
			matchBlocks."github.com" = {
				hostname = "github.com";
				identityFile = "/home/axolotl/.ssh/id_school";
			};
		};
	};
	systemd.user.services.ssh-agent = {
		Install.WantedBy = [ "default.target" ];
		Service = {
			ExecStart = with pkgs; writeShellScript "sshagent" ''
				rm /tmp/ssh-agent.sock; kill $(${procps}/bin/pidof ssh-agent)
				eval `${openssh}/bin/ssh-agent -a /tmp/ssh-agent.sock`
				${openssh}/bin/ssh-add $(${findutils}/bin/find /home/axolotl/.ssh -type f -name 'id_*' ! -name '*.pub')
			'';
			Type = "oneshot";
			RemainAfterExit = true;
		};
	};
}
