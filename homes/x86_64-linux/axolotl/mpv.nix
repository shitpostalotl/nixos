{ pkgs, ... }: {
	programs.mpv = {
		enable = true;
		scripts = with pkgs.mpvScripts; [ uosc sponsorblock ];
		config = {
			force-window = "immediate";
			vo = "gpu-next";
			gpu-api = "vulkan";
			#gpu-context = "wayland";
		};
		bindings = {
			"+" = "add volume 2";
			"-" = "add volume -2";
			"MBTN_LEFT" = "cycle pause";
			"MBTN_RIGHT" = "cycle fullscreen";
			"MBTN_LEFT_DBL" = "ignore";
			"Alt+j" = "add volume 10";
			"Alt+k" = "add volume -10";
			"Alt+h" = "seek 5";
			"Alt+l" = "seek -5";
			"h" = "seek 5";
			"j" = "seek -60";
			"k" = "seek 60";
			"l" = "seek -5";
		};
		scriptOpts.uosc = {
			timeline_persistency = "paused,audio";
			controls = "menu,gap,subtitles,<has_many_audio>audio,<has_many_video>video,<has_many_edition>editions,<stream>stream-quality,gap,space,shuffle,loop-playlist,loop-file,gap,prev,items,next";
			volume = "none";
			window_border_size = 0;
			pause_indicator = "none";
			top_bar = "always";
			top_bar_controls = "no";
		};
	};
}
