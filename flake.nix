{
	inputs = {
		nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
		snowfall-lib = {
			url = "github:snowfallorg/lib";
			inputs.nixpkgs.follows = "nixpkgs";
		};

		lix = {
			url = "https://git.lix.systems/lix-project/lix/archive/main.tar.gz";
			flake = false;
		};
		lix-module = {
			url = "git+https://git.lix.systems/lix-project/nixos-module";
			inputs.nixpkgs.follows = "nixpkgs";
			inputs.lix.follows = "lix";
		}; # https://git.lix.systems/lix-project/lix/src/branch/main/doc/manual/rl-next

		home-manager = { url = "github:nix-community/home-manager"; inputs.nixpkgs.follows = "nixpkgs"; };
		hosts = { url = "github:StevenBlack/hosts"; flake = false; };
		stylix = { url = "github:danth/stylix"; inputs.nixpkgs.follows = "nixpkgs"; };
		hardware.url = "github:NixOS/nixos-hardware/master";
		ersei.url = "sourcehut:~fd/nix-configs";

		googerteller-src = { url = "github:berthubert/googerteller"; flake = false; };
		neosurf-src = { url = "github:CobaltBSD/neosurf"; flake = false; };
		tilp-src = { url = "https://www.ticalc.org/pub/unix/tilp.tar.gz"; flake = false; };
		twinery-src = { url = "github:klembot/twinejs"; flake = false; };
		lush-src = { url = "github:BanceDev/lush"; flake = false; };
		tabs-kak-src = { url = "github:enricozb/tabs.kak"; flake = false; };
		luar-kak-src = { url = "github:gustavo-hms/luar"; flake = false; };
		_0xproto-src = { url = "github:0xType/0xProto"; flake = false; };
		scheme-lsp-server-src = { url = "git+https://codeberg.org/rgherdt/scheme-lsp-server"; flake = false; };
		vis-plug = { url = "github:erf/vis-plug"; flake = false; };

		left-uxn = { url = "sourcehut:~rabbits/left"; flake = false; };

		bandcampify.url = "git+https://codeberg.org/shitpostalotl/bandcampify.git";
		iceshrimp.url = "git+https://iceshrimp.dev/shitpostalotl/packaging?ref=dev&dir=iceshrimp-js";
	};

	outputs = inputs: inputs.snowfall-lib.mkFlake {
		inherit inputs;
		src = ./.;

		systems.hosts = let basicModules = with inputs; [ home-manager.nixosModules.default stylix.nixosModules.stylix lix-module.nixosModules.default ]; in {
			floortop.modules = with inputs.hardware.nixosModules; [ common-gpu-nvidia-nonprime common-gpu-intel-disable ] ++ basicModules;
			laptop.modules = with inputs.hardware.nixosModules; [ common-cpu-intel ] ++ basicModules;
			webserver.modules = with inputs; [ iceshrimp.nixosModules.iceshrimp hardware.nixosModules.common-cpu-intel ];
		};

		snowfall = {
			namespace = "xltl";
			meta = {
				name = "shitpostalotl";
				title = "Shitpostalotl's NixOS config";
			};
		};
	};
}
# TODO: https://github.com/keenanweaver/nix-config/blob/main/hosts/laptop/disko.nix https://github.com/keenanweaver/nix-config/blob/main/hosts/laptop/impermanence.nix https://github.com/nix-community/impermanence?tab=readme-ov-file#btrfs-subvolumes
# TODO: https://github.com/cynicsketch/nix-mineral
