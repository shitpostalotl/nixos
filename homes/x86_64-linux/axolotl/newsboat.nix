{ pkgs, lib, osConfig, ... }: let
	invidious = "inv.nadeko.net";
in {
	programs = {
		newsboat = {
			enable = true;
			browser = with pkgs; toString (writeShellScript "s" ''
				clear
				[[ "$1" =~ https://${invidious}.* ]] && {
					rm /tmp/vid/*
					mkdir -p /tmp/vid
					yt-dlp $(echo $1 | sed 's-https://${invidious}/watch?v=--;s/\&$//') -o "/tmp/vid/%(title)s.%(ext)s"
					mpv /tmp/vid/*
				} || {
					${osConfig.environment.variables.DEFAULT_BROWSER} $1
				}'');
			reloadThreads = 10;
			extraConfig = ''cache-file "~/Documents/.stuff/newsboatcache"'';
			urls = (import secrets/subs.nix) { inherit pkgs lib invidious; };
		};
		yt-dlp = {
			enable = true;
			settings = {
				sponsorblock-remove = "sponsor";
				force-ipv4 = true;
				concurrent-fragments = 10;
				embed-chapters = true;
			};
		};
	};
}
