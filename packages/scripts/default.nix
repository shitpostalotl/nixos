{ lib, pkgs, stdenv, ... }: stdenv.mkDerivation {
	name = "xltl-scripts";
	src = ./.;
	dontUnpack = true;
	installPhase = with lib; let
		name = path: last (builtins.split "/" (builtins.toString path));
		scripts = map (n: pkgs.writeScriptBin (name n) (import (builtins.toFile "${name n}.nix" ("pkgs: ''"+(builtins.readFile n)+"''")) pkgs )) (remove ./default.nix (fileset.toList ./.));
	in "mkdir -p $out/bin; "+(lib.concatMapStringsSep ";" (x: "cp ${x}/bin/* $out/bin") scripts);
}
