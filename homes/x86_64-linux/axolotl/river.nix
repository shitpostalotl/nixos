{ pkgs, lib, osConfig, host, ... }: with pkgs; {
	wayland.windowManager.river = {
		enable = true;
		settings = with osConfig.lib.stylix.colors; {
			background-color = "0x"+base00-hex;
			# Bind a bunch of keys
			map.normal = {
				# make my silly little keybinds to launch my silly little apps
				"Mod4 Space" = "spawn kitty";
				"Mod4+Shift Space" = "spawn 'tofi-drun --drun-launch=true'";
				"Mod4+Mod1 Space" = "spawn " + (writeShellScriptBin "toggle-bar" ''[ -e "$XDG_RUNTIME_DIR/sandbar" ] || mkfifo "$XDG_RUNTIME_DIR/sandbar"; echo all toggle-visibility > $XDG_RUNTIME_DIR/sandbar'');
				"Mod4 A" = "spawn "+osConfig.environment.variables.DEFAULT_BROWSER;
				"Mod4+Shift A" = "spawn logseq";
				"Mod4+Control A" = "spawn sioyek";
				"Mod4 S" = "spawn 'makoctl dismiss'";
				"Mod4+Shift S" = "spawn grim";
				"Mod4+Control S" = "spawn monitors";
				# brightness
				"Mod4 D" = "spawn 'bl +5'";
				"Mod4+Shift D" = "spawn 'bl -5'";
				"Mod4+Control D" = "spawn 'bl 75'";
				# river keybinds
				"Mod4 Q" = "close";
				"Mod4+Control Q" = "exit";
				"Mod4+Mod1 Q" = "spawn poweroff";
				# look
				"Mod4 H" = "focus-output left";
				"Mod4 J" = "focus-view next";
				"Mod4 K" = "focus-view previous";
				"Mod4 L" = "focus-output right";
				# move
				"Mod4+Shift H" = "send-to-output left";
				"Mod4+Shift J" = "swap next";
				"Mod4+Shift K" = "swap previous";
				"Mod4+Shift L" = "send-to-output right";
				"Mod4+Control K" = "send-layout-cmd luatile 'more_main(1)'";
				"Mod4+Control J" = "send-layout-cmd luatile 'more_main(-1)'";
				# resize
				"Mod4+Control L" = "send-layout-cmd luatile 'main_resize(5)'";
				"Mod4+Control H" = "send-layout-cmd luatile 'main_resize(-5)'";
				"Mod4 F" = "zoom";
				"Mod4+Shift F" = "toggle-fullscreen";
				# reset
				"Mod4+Control F" = "send-layout-cmd luatile 'main_resize(50,0)'";
				"Mod4+Mod1 F" = "send-layout-cmd luatile 'more_main(1,0)'";
			};
			rule-add."-app-id"."'*'" = "ssd";
			default-layout = "luatile";
			# Mouse-based window movement
			map-pointer.normal.Mod4 = {
				"BTN_LEFT" = "move-view";
				"BTN_RIGHT" = "resize-view";
				"BTN_MIDDLE" = "toggle-float";
			};
			set-cursor-warp = "on-focus-change";
			# Input device settings
			input = {
				"pointer-1739-52759-SYNA32A9:00_06CB:CE17_Touchpad" = {
					tap = true;
					click-method = "button-areas";
				};
				"touch-10900-53325-GTCH7503:00_2A94:D04D".events = false;
			};
			focus-follows-cursor = "normal";
			set-repeat = "50 300";
			# Other settings
			spawn = builtins.map (x: "'${x}'") [
				(writeShellScript "bar-stat" ''
					printf "%s" "$$" > "$XDG_RUNTIME_DIR/status_pid";FIFO="$XDG_RUNTIME_DIR/sandbar";[ -e "$FIFO" ] || mkfifo "$FIFO"
					while true; do sleep 1 & wait && {
						sec=$(date +%s)
						[ $(($sec % 5)) -eq 0 ] && (
							bat="$(cat /sys/class/power_supply/BAT0/capacity)%$([[ $(cat /sys/class/power_supply/BAT0/status) = "Charging" ]]&&echo +)"
							datetime="$(date "+%b %d (%a) %I:%M")"
							echo "all status $datetime${lib.optionalString (host == "laptop") " $bat"}" >"$FIFO"
						)
					}; done
				'')
				"monitors"
				(writeShellScript "bar" ''${lib.optionalString (host == "floortop") "sleep 1;"}FIFO="$XDG_RUNTIME_DIR/sandbar";[ -e "$FIFO" ] && rm -f "$FIFO";mkfifo "$FIFO";while cat "$FIFO"; do :; done | sandbar -font monospace:Pixelsize -active-bg-color '${withHashtag.base01}' -title-bg-color '${withHashtag.base01}' -inactive-bg-color '${withHashtag.base04}' -urgent-bg-color '${withHashtag.base08}' -active-fg-color '${withHashtag.base05}' -inactive-fg-color '${withHashtag.base05}' -urgent-fg-color '${withHashtag.base05}' -title-fg-color '${withHashtag.base05}' '')
				"river-luatile"
			];
		};
		xwayland.enable = true;
		package = river.override { xwaylandSupport = osConfig.home-manager.users.axolotl.wayland.windowManager.river.xwayland.enable; };
		systemd.enable = false;
		extraSessionVariables = {
			WEBKIT_DISABLE_COMPOSITING_MODE = "1";
			NIXOS_OZONE_WL = "1";
			ELECTRON_OZONE_PLATFORM_HINT = "auto";
			XDG_SESSION_TYPE = "wayland";
			WLR_NO_HARDWARE_CURSORS = "1";
			GTK_USE_PORTAL = "0";
		};
		extraConfig = with lib; let sqr = x: toString (foldl' builtins.mul 1 (replicate x 2)); in concatMapStrings (x: "riverctl map normal Mod4${x};") (concatMap (x: [ " ${toString (x+1)} set-focused-tags ${sqr x}" "+Control ${toString (x+1)} toggle-focused-tags ${sqr x}" "+Shift ${toString (x+1)} set-view-tags ${sqr x}" "+Shift+Control ${toString (x+1)} toggle-view-tags ${sqr x}" ]) (range 0 8));
	};
	home.packages = with pkgs; [ river-luatile sandbar ];
	xdg.configFile."river-luatile/layout.lua".source = ./luatile.lua;
}
