{ config, lib, pkgs, ... }: {
	options.services.nfssh = with lib; {
		enable = mkEnableOption "nfssh mounting";
		servers = mkOption {
			description = "Per-server mount configuration.";
			default = {};
			type = with types; attrsOf (submodule { options = {
				server = mkOption {
					description = "Settings for the server the drive is hosted on";
					default = {};
					type = submodule { options = {
						user = mkOption {
							description = "user to ssh into";
							default = null;
							example = "nasUser";
							type = str;
						};
						port = mkOption {
							description = "port from server to bind";
							default = 2049;
							example = 6969;
							type = coercedTo port builtins.toString str;
						};
					};};
				};
				local = mkOption {
					description = "Settings for the system the drive is mounted on";
					default = {};
					type = submodule { options = {
						user = mkOption {
							description = "user to run systemd service as";
							default = null;
							example = "nasUser";
							type = str;
						};
						port = mkOption {
							description = "port on client to bind to";
							default = 2049;
							example = 6969;
							type = coercedTo port builtins.toString str;
						};
					};};
				};
				mounts = mkOption {
					description = "Mountpoints in the form of { local = remote; }";
					default = {};
					type = attrsOf str;
				};
			};});
		};
	};

	config = with lib; mkIf config.services.nfssh.enable {
		boot = {
			kernelModules = [ "nfs" ];
			supportedFilesystems = [ "nfs4" ];
		};
		services = {
			udisks2.settings."mount_options.conf".defaults.ntfs_defaults = "uid=$UID,gid=$GID,noatime,prealloc";
			rpcbind.enable = true;
		};
		environment.systemPackages = [ pkgs.nfs-utils ];
		systemd = let
			mountSettings = lib.concatLists (lib.mapAttrsToList (url: conf: lib.mapAttrsToList (l: r: { local = l; remote = r; port = conf.local.port; name = lib.escapeURL (lib.replaceStrings ["."] ["-"] url)+"-tunnel"; }) conf.mounts) config.services.nfssh.servers);
		in {
			services = lib.mapAttrs' (url: conf: lib.nameValuePair (
				(lib.escapeURL (lib.replaceStrings ["."] ["-"] url))+"-tunnel")
				{
					description ="Create tunnel for nfssh server "+url;
					after = [ "network-online.target" ];
					wants = [ "network-online.target" ];
					wantedBy = [ "multi-user.target" ];
					serviceConfig = {
						Type = "simple";
						User = conf.local.user;
						Restart = "always";
						ExecStartPre = "/bin/sh -c 'until ${pkgs.unixtools.ping}/bin/ping -c1 ${url}; do sleep 1; done;'";
						ExecStart = with pkgs; "${openssh}/bin/ssh -o 'UpdateHostKeys=no' -NL ${builtins.toString conf.server.port}:localhost:${builtins.toString conf.local.port} ${conf.server.user}@${url}";
					};
				}
			) config.services.nfssh.servers;
			mounts = lib.map (x: {
				type = "nfs4";
				mountConfig.Options = "noatime,nfsvers=4.1,proto=tcp,nolock,port=${builtins.toString x.port}";
				what = "localhost:"+x.remote;
				where = toString x.local;
			}) mountSettings;
			automounts = map (x: {
				wantedBy = [ "multi-user.target" ];
				after = [ (x.name+".target") ];
				automountConfig.TimeoutIdleSec = "600";
				where = toString x.local;
			}) mountSettings;
		};
	};
}
