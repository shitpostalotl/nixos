{ pkgs, inputs, ... }:
pkgs.stdenv.mkDerivation {
	pname = "tilp";
	version = "v1.18";
	src = inputs.tilp-src;
	nativeBuildInputs = with pkgs; [ autoreconfHook pkg-config intltool libtifiles2 libticalcs2 libticables2 libticonv gtk2 ];
	buildInputs = with pkgs; [ glib ];
	postPatch = ''
		sed -i '/AC_PATH_KDE/d' configure.ac
		sed -i -e 's/@[^@]*\(KDE\|QT\|KIO\)[^@]*@//g' -e 's/@X_LDFLAGS@//g' src/Makefile.am
	'';
}
