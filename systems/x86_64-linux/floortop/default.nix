{ config, pkgs, lib, modulesPath, ... }: {
	imports = [ (modulesPath + "/installer/scan/not-detected.nix") ../global ];

	nix.settings = {
		substituters = [ "https://cuda-maintainers.cachix.org" ];
		trusted-public-keys = [ "cuda-maintainers.cachix.org-1:0dq3bujKpuEPMCX6U4WylrUDZ9JyUG0VpVZa7CNfq5E=" ];
	};

	boot = {
		initrd.kernelModules = [ "kvm-intel" "nvidia" "nvidia_modeset" "nvidia_drm" "nvidia_uvm" ];
		initrd.availableKernelModules = [ "xhci_pci" "ahci" "nvme" "usbhid" ];
		extraModprobeConfig = "options nvidia_drm modeset=1 fbdev=1";
	};

	fileSystems = {
		"/" = {
			device = "/dev/disk/by-uuid/26273615-6c4d-49fa-9054-22a07bf0e965";
			fsType = "ext4";
		};
		"/boot/efi" = {
			device = "/dev/disk/by-uuid/56DD-37FF";
			fsType = "vfat";
		};
		"/games" = {
			device = "/dev/disk/by-uuid/d53e1d84-2bac-4731-93f8-ef5330ac92f9";
			fsType = "ext4";
		};
	};

	services.xserver.videoDrivers = [ "nvidia" ];
	hardware.nvidia = {
		package = config.boot.kernelPackages.nvidiaPackages.latest;
		powerManagement.enable = true;
		open = true;
	};
	nixpkgs.config = {
		allowUnfree = true;
		cudaSupport = true;
	};

	home-manager.users.axolotl.wayland.windowManager.river.extraSessionVariables = {
		LIBVA_DRIVER_NAME = "nvidia";
		GBM_BACKEND = "nvidia-drm";
		__GLX_VENDOR_LIBRARY_NAME = "nvidia";
		CUDA_PATH = builtins.toString pkgs.cudatoolkit;
		LD_LIBRARY_PATH = "/run/opengl-driver/lib:/run/opengl-driver-32/lib";
	};

	swapDevices = [ { device = "/dev/disk/by-uuid/45c5889b-9437-4797-9f8e-65faf80f506c"; } ];

	networking = {
		hostName = "floortop";
		wired = true;
	};
	powerManagement.cpuFreqGovernor = "performance";
	hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
