{ osConfig, lib, inputs, host, ... }: {
	imports = if host != "webserver" then with lib; map (x: if builtins.functionArgs (import x) == {inputs=false;} then import x {inherit inputs;} else x) (lib.snowfall.fs.get-non-default-nix-files ./.) ++ map (x: x.value.homeModule or x.value.homeModules.${x.name} or x.value.hmModules.${x.name}) (filter (x: x.value?homeModule || x.value?homeModule || x.value?hmModules) (attrsToList inputs)) else [];
	home.stateVersion = osConfig.system.stateVersion;
	xdg.userDirs = { enable = true; } // lib.genAttrs [ "desktop" "download" "music" "pictures" ] (_: osConfig.users.users.axolotl.home);
}
