{ ... }: {
	services.keyd = {
		enable = true;
		keyboards.default = {
			ids = [ "*" ];
			settings = {
				main = {
					"y" = "-";
					"u" = "y";
					"i" = "u";
					"o" = "i";
					"p" = "o";
					"[" = "p";
					"]" = "[";
					"\\" = "]";

					"h" = ";";
					"j" = "h";
					"k" = "j";
					"l" = "k";
					";" = "l";

					"n" = "/";
					"m" = "n";
					"," = "m";
					"." = ",";
					"/" = ".";

					"1" = "\\";
					"2" = "1";
					"3" = "2";
					"4" = "3";
					"5" = "4";
					"6" = "5";
					"7" = "=";
					"8" = "6";
					"9" = "7";
					"0" = "8";
					"-" = "9";
					equal = "0";

					capslock = "overload(meta, esc)";
					rightcontrol = "capslock";
					rightalt = "layer(nav)";
					esc = "noop";
				};
				nav = {
					h = "backspace";
					j = "left";
					k = "down";
					l = "up";
					";" = "right";
					m = "home";
					"," = "pagedown";
					"." = "pageup";
					"/" = "end";
					z = "noop";
				};
			};
		};
	};
}
# TODO: write out an ahk script for when i am forced to use windows computer
# don't bother with overloads
