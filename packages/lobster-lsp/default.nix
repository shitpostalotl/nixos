{ pkgs, ... }:
pkgs.buildNpmPackage {
	pname = "lobster_lsp";
	version = "main";
	#src = pkgs.lobster.src
	src = pkgs.fetchFromGitHub {
		owner = "aardappel";
		repo = "lobster";
		rev = "7fc2864";
		sha256 = "sha256-DWNw9Nbas7dTIkH8RS58TjGHw4htdYKANDEXz3ba2ss=";
	} + "/dev/lsp";
	npmBuildScript = "webpack:dev";
	npmDepsHash = "sha256-AirWSNuwLljj7hl6xZWS6BU7JADkMryrlQYFT5lZORQ=";
	installPhase = "mkdir $out; cp -r * $out; makeWrapper ${pkgs.nodejs_23}/bin/node $out/bin/lobster-lsp --add-flags $out'/webpack-out/lobster_lsp.js'";
	nativeBuildInputs = [ pkgs.lobster ];
}
