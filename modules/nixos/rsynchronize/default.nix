{ config, lib, pkgs, ... }: {
	options.services.rsynchronize = with lib; {
		enable = mkEnableOption "rsynchronize mounting";
		drives = mkOption {
			description = "List of drive settings.";
			default = [];
			example = [{ server = {url = "url (or ip) of the ssh server"; user = "user to ssh into (note to omit trailing slash)"; path = "path to sync";}; local = {user = "user to run systemd service as"; path = "path to sync to/from (note to omit trailing slash)";};}];
			type = with types; listOf (submodule { options = {
				upload = mkOption {
					description = "Enable uploading the files.";
					example = false;
					type = bool;
					default = true;
				};
				download = mkOption {
					description = "Enable downloading the files.";
					example = false;
					type = bool;
					default = true;
				};
				shaCheck = mkEnableOption "checking shasum of drive to detect changes";
				server = mkOption {
					description = "Settings for the ssh server the files are on.";
					default = {};
					example = {url = "url (or ip) of the ssh server"; user = "user to ssh into"; path = "path to sync";};
					type = submodule { options = {
						url = mkOption {
							description = "url (or ip) of the ssh server";
							default = null;
							example = "mysshishere.com";
							type = str;
						};
						user = mkOption {
							description = "user to ssh into";
							default = null;
							example = "sshUser";
							type = str;
						};
						path = mkOption {
							description = "path to sync (note to omit leading and trailing slash)";
							default = null;
							example = "volume1/stuff";
							type = strMatching "[^/]*(/[^/]*)*[^/]";
						};
					};};
				};
				local = mkOption {
					description = "Settings for the local system the files are synced to/from";
					default = {};
					example = {user = "user to run systemd service as"; path = "path to sync to/from (note to omit trailing slash)";};
					type = submodule { options = {
						user = mkOption {
							description = "user to run systemd service as";
							default = null;
							example = "nasUser";
							type = str;
						};
						path = mkOption {
							description = "absolute path to sync to/from (note to omit trailing slash)";
							default = null;
							example = "stuff";
							type = strMatching "(/[^/]*)*[^/]";
						};
					};};
				};
			};});
		};
	};

	config = with lib; mkIf config.services.rsynchronize.enable {
		systemd.services = listToAttrs (map (d: { name = (baseNameOf d.local.path)+"Rsynchronize"; value = {
			description = "Rsynchronize folder "+(baseNameOf d.local.path);
			after = [ "network-online.target" ];
			wants = [ "network-online.target" ];
			wantedBy = [ "multi-user.target" ];
			serviceConfig = with pkgs; let
				command = origin: dest: ''${rsync}/bin/rsync -e '${openssh}/bin/ssh -o "NumberOfPasswordPrompts 0"' -va --no-g --delete ${origin} ${dest}'';
				getsha = dir: ''${findutils}/bin/find ${dir} -type f -exec ${bash}/bin/bash -c "${coreutils}/bin/sha256sum \"{}\"" \;'';
				shafile = "/tmp/$(echo ${d.local.path} | ${gnused}/bin/sed 's/\//_/g').sha";
			in {
				Type = "oneshot";
				User = d.local.user;
				RemainAfterExit = true;
				ExecStart = optionalString d.download (writeShellScript "backdown.sh" (command "${d.server.user}@${d.server.url}::${d.server.path}/" d.local.path)+(optionalString d.shaCheck "; rm ${shafile d.local.path}; ${getsha d.local.path} > ${shafile d.local.path}"));
				ExecStop = optionalString d.upload (writeShellScript "backup.sh" ((optionalString d.shaCheck "[[ $(${diffutils}/bin/diff ${shafile d.local.path} <(${getsha d.local.path})) ]] && ")+(command "${d.local.path}/" "${d.server.user}@${d.server.url}::${d.server.path}")));
			};
		};}) config.services.rsynchronize.drives);
	};
}
