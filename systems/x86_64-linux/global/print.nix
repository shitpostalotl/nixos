{ pkgs, ... }: {
	services.printing = {
		enable = true;
		stateless = true;
		drivers = with pkgs; [ gutenprint hplip ];
	};

	hardware.printers.ensurePrinters = [
		{
			name = "orangejuice";
			location = "Mom's house";
			deviceUri = "ipp://10.0.0.76/ipp/print";
			model = "drv:///hp/hpcups.drv/hp-officejet_8020_series.ppd";
			ppdOptions.Duplex = "DuplexNoTumble";
		}
		{
			name = "envious";
			location = "Dad's house";
			deviceUri = "ipp://192.168.0.6/ipp/print";
			model = "drv:///sample.drv/generic.ppd";
			ppdOptions.Duplex = "DuplexNoTumble";
		}
		{
			name = "fuck";
			location = "School";
			deviceUri = "ipp://10.220.3.39/ipp/print";
			model = "drv:///sample.drv/generic.ppd";
			ppdOptions.Duplex = "DuplexNoTumble";
		}
	];
}
