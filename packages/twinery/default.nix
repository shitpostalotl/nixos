{ pkgs, inputs, ... }:
pkgs.buildNpmPackage {
	pname = "twinery";
	version = "main";
	src = inputs.twinery-src;
	npmDepsHash = "sha256-LXLKOC9ZlnmQ2uRJiW4MNzLSi9PscGoIZ/6hxz0FZws=";
	prePatch = "sed 's/ --serial build:electron-bundle//' -i package.json";
	npmBuildScript = "build:electron";
	installPhase = ''mkdir -p $out/bin $out/e;cp -r electron-build/main/* node_modules dist/web $out/e;cp -r electron-build/renderer $out;${pkgs.jq}/bin/jq -aM '.homepage = "'$out'/e/web" | .main = "'$out'/e/src/electron/main-process/index.js"' package.json > $out/e/package.json''; # find a way to fix the missing files thing or at the very least put sample files somewhere
	postFixup = ''makeWrapper ${pkgs.electron}/bin/electron $out/bin/twine --add-flags $out/e --set NODE_PATH $out/e/node_modules --add-flags "\''${NIXOS_OZONE_WL:+\''${WAYLAND_DISPLAY:+--ozone-platform-hint=auto --enable-features=WaylandWindowDecorations}}"'';
	env = {
		ESBUILD_BINARY_PATH = with pkgs; (esbuild.override {buildGoModule=args: buildGoModule (args // {src=fetchFromGitHub {owner="evanw";repo="esbuild";rev="refs/tags/v0.21.5";hash="sha256-FpvXWIlt67G8w3pBKZo/mcp57LunxDmRUaCU/Ne89B8=";};vendorHash="sha256-+BfxCyg0KkDQpHt/wycy/8CTG6YBA/VJvJFhhzUnSiQ=";});})+"/bin/esbuild";
		ELECTRON_SKIP_BINARY_DOWNLOAD = "1";
	};
}
