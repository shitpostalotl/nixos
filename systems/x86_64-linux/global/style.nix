{ config, pkgs, ... }: with pkgs; {
	fonts.packages = [ unifont unifont_upper ];
	stylix = {
		enable = true;
		image = fetchurl {url="https://github.com/rose-pine/wallpapers/blob/2881ffd/blockwavemoon.png?raw=true";sha256="sha256-VenNP2aJ55hU8AfqZ4KHzTkiq+9GveHqL69vgSmRPlE=";};
		base16Scheme = "${base16-schemes}/share/themes/rose-pine.yaml";
		fonts = let
			font = { package = ibm-plex; name = "IBM Plex"; };
		in {
			monospace = { package = xltl._0xproto; name = "0xProto"; };
			sansSerif = font;
			emoji = font;
			serif = font;
			sizes = if config.networking.hostName != "floortop" then { applications = 10; terminal = 9; } else { applications = 12; terminal = 13; };
		};
	};
}
