# My NixOS config
Configuration for my various NixOS systems.

## Deployment:
1. Install [NixOS](https://nixos.org), making sure to install `git` and `git-crypt`
2. Generate a set of SSH keys (`ssh-keygen -t ed25519 -a 100 -f ~/.ssh/id_ed25519`)
3. Copy the SSH public key to Codeberg, UNIX.dog, and the NAS
4. Clone this repository via SSH
5. Unlock the `git-crypt` encryption
6. Copy the `hardware-configuration.nix` to an appropriately named file in `hosts`, taking care to format and remove redundancy
