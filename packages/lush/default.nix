{ pkgs, inputs, ... }:
pkgs.stdenv.mkDerivation {
	pname = "lush";
	version = "main";
	src = inputs.lush-src;
	nativeBuildInputs = with pkgs; [ lua54Packages.lua premake5 gnumake ];
	configurePhase = "sed -i 's-\.lush/-\.config/lush/-g' .lush/scripts/example.lua src/history.c src/lua_api.c";
	buildPhase = "premake5 gmake; make";
	installPhase = "mkdir -p $out/bin; cp bin/Debug/lush/lush $out/bin; cp -r .lush $out/bundled";
}
