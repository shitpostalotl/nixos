{ config, lib, ... }:
with lib; {
	options.services.realfiles = mkOption {
		type = with types; attrsOf (strMatching "(/[^/]*)*[^/]");
		default = {};
		description = "The real files you want to create";
		example = literalExpression ''{\n"/destination" = "/source";\n"/destination" = "/source";\n"/destination" = "/source";\n};'';
	};

	config.systemd = {
		tmpfiles.rules = mapAttrsToList (dest: src: "L ${dest} - - - - ${src}") config.services.realfiles;
		services.systemd-tmpfiles-resetup.wantedBy = [ "sysinit-reactivation.target" ];
	};
}
