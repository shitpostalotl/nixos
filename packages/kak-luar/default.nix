{ pkgs, inputs, ... }:
pkgs.stdenv.mkDerivation {
	pname = "luar";
	version = "main";
	src = inputs.luar-kak-src;
	installPhase = let
		rtpPath = "share/kak/autoload/plugins";
	in ''
		mkdir -p $out/${rtpPath}
		cp -r . $out/${rtpPath}/luar
	'';
}
