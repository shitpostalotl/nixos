{ pkgs, inputs, ... }: {
	programs.uxn = {
		enable = true;
		enableLauncher = true;
		roms = with pkgs; [
			(stdenv.mkDerivation {
				name = "left";
				src = inputs.left-uxn;
				nativeBuildInputs = [ uxn ];
				makeFlags = [ "DIR=$(out)" "PREFIX=$(out)" "ASM=uxnasm" ];
			})
		];
	};
}
