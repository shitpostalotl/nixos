{ config, pkgs, lib, inputs, ... }: {
	zramSwap.enable = true;
	services = {
		getty = {
			greetingLine = ''${config.networking.hostName} running NixOS version ${config.system.nixos.release} (${config.system.nixos.codeName})'';
			helpLine = "THIS MACHINE BITES FASCISTS";
			extraArgs = ["-J" "-N" "--nohostname"];
			autologinUser = if config.networking.hostName == "laptop" then "axolotl" else null;
		};
		pipewire = {
			enable = true;
			alsa.enable = true;
			pulse.enable = true; # steam wants this
		};
		libinput.enable = true;
		udev.extraRules = ''
			ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", MODE="0666", RUN+="${pkgs.coreutils}/bin/chmod a+w /sys/class/backlight/%k/brightness"
			ACTION=="add", SUBSYSTEMS=="usb", SUBSYSTEM=="block", ENV{ID_FS_USAGE}=="filesystem", ENV{ID_SERIAL_SHORT}=="121220160204", RUN{program}+="${pkgs.systemd}/bin/systemd-mount --owner axolotl --no-block -A --timeout-idle-sec=1s -G $devnode /mnt/sd"
		'';
		rsynchronize = {
			enable = true;
			drives = [{
				server = { url = "nas.local"; user = "axolotl"; path = "axolotl"; };
				local = { user = "axolotl"; path = "/home/axolotl/Documents"; };
			}];
		};
	};
	specialisation.noBackup.configuration.services.rsynchronize.enable = lib.mkForce false;

	networking = {
		useDHCP = true;
		nameservers = [ "9.9.9.9" "2620:fe::9" ];
		# AP classroom wants split.io
		hosts."0.0.0.0"=with builtins;with lib;map (x: removePrefix "0.0.0.0 " x) (filter (f: (hasPrefix "0.0.0.0" f) && !(hasSuffix ".split.io" f)) (splitString "\n" (readFile (inputs.hosts.outPath+"/hosts"))));
		nw-hosts = {
			"thor95*" = {
				"10.0.0.85" = [ "nas.local" ];
				"10.0.0.136" = [ "axfedi.derg.rest" ];
			};
			"*" = {
				"73.202.224.102" = [ "nas.local" ];
			};
		};
	};

	security = {
		rtkit.enable = true;
		sudo.enable = false;
		doas = {
			enable = true;
			extraRules = [ { groups = [ "wheel" ]; noPass = false; persist = true; keepEnv = true; } ];
		};
	};
}
