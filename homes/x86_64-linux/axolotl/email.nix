{ pkgs, ... }: {
	programs.aerc = {
		enable = true;
		extraConfig = {
			general = {
				unsafe-accounts-conf = true;
				source = "imap://shitpostalotl@unix.dog";
				outgoing = "smtp://shitpostalotl@unix.dog";
			};
			filters = let preins = f: pkgs.aerc+"/libexec/aerc/filters/"+f; in {
				"text/html" = preins "html";
				"text/*" = preins "plaintext";
			};
			ui = {
				fuzzy-complete = true;
				threading-enabled = true;
			};
			compose.address-book-cmd = "grep -iFs '%s' /home/axolotl/Documents/contacts.tsv";
		};
	};
	accounts.email.accounts.shitpostalotl = {
		primary = true;
		address = "shitpostalotl@unix.dog";
		realName = "shitpostalotl"; userName = "shitpostalotl";
		passwordCommand = "passage unixdog | head -n1";
		smtp.host = "smtp.unix.dog";
		imap.host = "imap.unix.dog";
		aerc.enable = true;
	};
}
