{ ... }: {
	programs.nnn = {
		enable = true;
		bookmarks = {
			x = "/etc/nixos";
			s = "/etc/nixos/systems/x86_64-linux/global";
			h = "/etc/nixos/homes/x86_64-linux/axolotl";
			a = "~";
		};
		plugins = {
			mappings = {};
			src = null;
		};
	};
}
