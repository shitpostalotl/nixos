-- GLOBAL SETTINGS
settings = setmetatable( {}, { __index = function(_, mon)
	if rawget(settings,mon) == nil then
		rawset(settings, mon, setmetatable( {}, { __index = function(_, tag)
			if rawget(settings[mon],tag) == nil then
				rawset(settings[mon], tag, { ratio = 0.5, count = 1 })
			end
			return rawget(settings[mon],tag)
		end } ) )
	end
	return rawget(settings,mon)
end } )

-- riverctl send-layout-cmd luatile "fn()"
-- Active tags = `CMD_TAGS`; Output name `CMD_OUTPUT`; `relative` = 1 or 0
function main_resize(amount, relative)
	relative = relative == nil and 1 or relative
	settings[CMD_OUTPUT][CMD_TAGS].ratio = math.min(math.max((settings[CMD_OUTPUT][CMD_TAGS].ratio*relative) + (amount * 0.01),0.01),0.99)
end
function more_main(amount, relative)
	relative = relative == nil and 1 or relative
	settings[CMD_OUTPUT][CMD_TAGS].count = math.max((settings[CMD_OUTPUT][CMD_TAGS].count*relative) + amount,1)
end

-- UTILITY FUNCTIONS
function tprint(tbl, indent)
	if not indent then indent = 0 end
	for k, v in pairs(tbl) do
		formatting = string.rep("  ", indent) .. k .. ": "
		if type(v) == "table" then
			print(formatting)
			tprint(v, indent+1)
		elseif type(v) == 'boolean' then
			print(formatting .. tostring(v))		
		else
			print(formatting .. v)
		end
	end
end

function tcopy(t)
	local u = { }
	for k, v in pairs(t) do u[k] = v end
	return setmetatable(u, getmetatable(t))
end

function tconcat(t1,t2)
	for _,v in ipairs(t2) do table.insert(t1, v) end
	return t1
end

-- LAYOUTS
function even(args, stripe, offset_x, offset_y)
	local retval = {}
	local portrait = math.floor(args.width / args.height)
	local landscape = math.floor(args.height / args.width)
	local window_h = math.ceil(stripe.height / stripe.count)
	local window_w = math.ceil(stripe.width / stripe.count)
	for i = 1, stripe.count do
		table.insert(retval, {
			offset_x + (i - 1) * window_w * landscape,
			offset_y + (i - 1) * window_h * portrait,
			stripe.width * portrait + window_w * landscape,
			stripe.height * landscape + window_h * portrait
		})
	end
	return retval
end

function dwindle(args, stripe, offset_x, offset_y)
	local portrait = math.floor(stripe.width / stripe.height)
	local landscape = math.floor(stripe.height / stripe.width)
	local first = ((stripe.count == args.count - settings[args.output][args.tags].count and stripe.count > 1) and 1 or 0)
	stripe.count = stripe.count - 1
	stripe.height = math.ceil(stripe.height - (stripe.height / 2) * landscape * ((stripe.count == 0) and 0 or 1))
	stripe.width = math.ceil(stripe.width - (stripe.width / 2) * portrait * ((stripe.count == 0) and 0 or 1))
	local new = {
		args.width - stripe.width * portrait * ((stripe.count == 0) and 0 or 1) - stripe.width,
		args.height - stripe.height * landscape * ((stripe.count == 0) and 0 or 1) - stripe.height + first,
		stripe.width,
		stripe.height - first
	}
	if stripe.count == 0 then
		return {new}
	else
		return tconcat({new}, dwindle(args, stripe, offset_x, offset_y))
	end
end

function outside(args, main_layout, side_layout)
	local landscape = math.floor(args.width / args.height)
	local count_safe = math.max(math.min(settings[args.output][args.tags].count, args.count-1),1)
	local main_ratio = settings[args.output][args.tags].ratio
	local main_stripe = tcopy(args)
	local side_stripe = tcopy(args)
	if landscape == 1 then
		main_stripe.width = args.width * main_ratio
		side_stripe.width = args.width * (1 - main_ratio)
	elseif landscape == 0 then
		main_stripe.height = args.height * main_ratio
		side_stripe.height = args.height * (1 - main_ratio)
	end
	main_stripe.count = count_safe
	side_stripe.count = args.count - count_safe
	return tconcat(main_layout(args, main_stripe, 0, 0), side_layout(args, side_stripe, main_stripe.width * landscape, main_stripe.height * ((landscape == 1) and 0 or 1)))
end

function handle_layout(args) -- args = { tags (focused), count (of windows), output, width (of mon), height (of mon)}
	if args.count == 1 then
		return {{ 0, 0, args.width, args.height }}
	elseif args.count > 1 then
		local layout = outside(args, even, dwindle)
		return layout -- Must return table with `count` entries. Each entry is { X cord, Y cord, Window width, Window height }
	end
end

-- This optional function returns the metadata for the current layout.
-- Currently only `name` is supported, the name of the layout. It get's passed the same `args` as handle_layout()
function handle_metadata(args) return { name = "|-" } end
