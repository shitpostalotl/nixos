{ pkgs, lib, inputs, ... }: {
	nixpkgs.config = {
		allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [ "steam" "steam-run" "steam-original" "steam-unwrapped" "slack" ];
		permittedInsecurePackages = [ "electron-27.3.11" ];
	};
	nix = {
		optimise.automatic = true;
		gc = {
			automatic = true;
			dates = "weekly";
		};
		settings  = {
			auto-optimise-store = true;
			trusted-users = [ "root" "@wheel" ];
			experimental-features = [ "nix-command" "flakes" "pipe-operators" ];
			substituters = [ "https://cache.lix.systems/" "https://cuda-maintainers.cachix.org/" "https://nix-community.cachix.org/" ];
			trusted-public-keys = [ "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY=" "cache.lix.systems:aBnZUw8zA7H35Cz2RyKFVs3H4PlGTLawyY5KRbvJR8o=" "cuda-maintainers.cachix.org-1:0dq3bujKpuEPMCX6U4WylrUDZ9JyUG0VpVZa7CNfq5E=" "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs=" ];
		};
	};

	environment = with pkgs; {
		defaultPackages = [ xdg-utils libsecret libnotify ];
		systemPackages = [
			# CLI UTILS
			git-crypt
			magic-wormhole-rs
			xltl.scripts
			tealdeer
			jellyfin-tui
			# GUI UTILS
			pwvucontrol
			qbittorrent
			slack
			anki texlive.combined.scheme-full
			# OFFICE
			lyx
			sioyek
			# ART
			blender
			inputs.ersei.packages.${pkgs.system}.gimp-devel
			inkscape
		];
		shellAliases = with pkgs; lib.mkForce {
			bc = gavin-bc+"/bin/bc -l";
			df = duf+"/bin/duf";
			d = xdragon+"/bin/dragon *";
			rm = rmtrash+"/bin/rmtrash";
			erm = coreutils+"/bin/rm";
			rg = ripgrep+"/bin/rg --hyperlink-format=kitty";
			rb = "doas nixos-rebuild";
			ls = "eza";
			nr = "nix repl -f \"<nixpkgs>\"";
			lb = "lsblk";
			j = "journalctl -b -f -u";
			img = "kitten icat";
			clear = "kitten @ action --self clear_terminal scrollback active; \clear";
		};
	};

	programs = {
		virt-manager.enable = true;
		obs-studio = {
			enable = true;
			enableVirtualCamera = true;
			plugins = [ pkgs.obs-studio-plugins.wlrobs ];
		};
		gamemode.enable = true;
		steam.enable = true; # sometimes wants --reset
		dconf.enable = true;
		htop.enable = true;
		git.enable = true;
	};

	virtualisation.libvirtd.enable = true;
}
