{ config, pkgs, lib, ... }: {
	users = {
		users.axolotl = {
			isNormalUser = true;
			description = "axolotl";
			hashedPassword = "$6$KYYG0h258oYZS6af$FtmE00R8jZpKkyI8DX6mR4ZVfZBjszEMM2Fa/8ZXHRJQPN.R42EOkgSGnDvrFGgaTB/vjSVKnyFIw5ObTVhhh/";
			extraGroups = [ "tty" "dialout" "libvirtd" "video" "wheel" ];
			home = "/home/axolotl";
			shell = pkgs.zsh;
		};
		mutableUsers = false;
	};
	programs.zsh.enable = true;

	xdg = {
		portal = {
			enable = true;
			wlr = {
				enable = true;
				settings.screencast = {
					chooser_type = "simple";
					chooser_cmd = "${pkgs.slurp}/bin/slurp -f %o -ro";
				};
			};
			xdgOpenUsePortal = true;
			config.common.default = "*";
			extraPortals = with pkgs; [ xdg-desktop-portal-wlr ];
		};
		mime = {
			enable = true;
			defaultApplications = lib.genAttrs [ "text/html" "x-scheme-handler/http" "x-scheme-handler/https" "x-scheme-handler/about" "x-scheme-handler/unknown" ] (_: config.environment.variables.DEFAULT_BROWSER+".desktop");
		};
	};

	environment.variables = config.home-manager.users.axolotl.programs.password-store.settings // {
		# user
		EDITOR = "kak";
		SSH_AUTH_SOCK = "/tmp/ssh-agent.sock";
		DEFAULT_BROWSER = "qutebrowser";
		NOTEBOOK = config.users.users.axolotl.home + "/Documents/notebook";
		# homedir
		HISTFILE = config.users.users.axolotl.home + "/.local/state/bashhist";
		GTK2_RC_FILES = config.users.users.axolotl.home + "/.config/gtk-2.0/gtkrc";
		ICEAUTHORITY = config.users.users.axolotl.home + "/.cache/ICEauthority";
		PYTHONSTARTUP = "/etc/python/pythonrc";
		DOT_SAGE = config.users.users.axolotl.home + "/.config/sage";
		XCOMPOSECACHE = config.users.users.axolotl.home + "/.cache/X11/xcompose";
		XAUTHORITY = "$XDG_RUNTIME_DIR/Xauthority";
		XDG_DOWNLOAD_DIR = config.users.users.axolotl.home;
		WINEPREFIX = config.users.users.axolotl.home + "/.local/share/wine";
	};
}
