{ config, lib, modulesPath, ... }:
{
	imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

	boot = {
		initrd.availableKernelModules = [ "xhci_pci" "ahci" "usbhid" "sd_mod" "rtsx_pci_sdmmc" ];
		kernelModules = [ "kvm-intel" ];
	};

	fileSystems = {
		"/" = {
			device = "/dev/disk/by-uuid/d23a2cd1-eabe-4c7e-9264-aade0fad6cd5";
			fsType = "ext4";
		};
		"/boot/efi" = {
			device = "/dev/disk/by-uuid/9515-6BA9";
			fsType = "vfat";
		};
	};

	networking.useDHCP = lib.mkDefault true;

	nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
	hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
