{ pkgs, inputs, ... }:
pkgs.stdenv.mkDerivation {
	pname = "googerteller";
	version = "main";
	src = inputs.googerteller-src;
	nativeBuildInputs = with pkgs; [ cmake pcaudiolib ];
	installPhase = let
		global=with pkgs; writeShellScript "g" "$1 ${tcpdump}/bin/tcpdump -nql | $(dirname $0)/googerteller";
	in "mkdir -p $out/bin; cp teller $out/bin/googerteller; cp ${global} $out/bin/googer-global";
}
