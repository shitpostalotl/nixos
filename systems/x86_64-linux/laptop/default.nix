{ modulesPath, ... }: {
	imports = [ (modulesPath + "/installer/scan/not-detected.nix") ../global ];
	networking.hostName = "laptop";

	boot = {
		kernelParams = [ "i8042.nopnp=1" "i8042.dumbkbd=1" ];
		kernelModules = [ "kvm-intel" ];
		swraid.enable = false;
		initrd = {
			availableKernelModules = [ "xhci_pci" "vmd" "nvme" "usb_storage" "sd_mod" "rtsx_pci_sdmmc" ];
			secrets."/crypto_keyfile.bin" = null; # Setup keyfile
			luks.devices = { # Enable swap on luks
				"luks-c26a9eb8-d2d9-4ea8-8883-7aafd08575ec".device = "/dev/nvme0n1p2";
				"luks-cf41dbfa-018e-40ed-a441-69dea7c69f27".device = "/dev/nvme0n1p3";
				"luks-cf41dbfa-018e-40ed-a441-69dea7c69f27".keyFile = "/crypto_keyfile.bin";
			};
		};
	};

	fileSystems = {
		"/" = {
			device = "/dev/mapper/luks-c26a9eb8-d2d9-4ea8-8883-7aafd08575ec";
			fsType = "ext4";
		};
		"/boot/efi" = {
			device = "/dev/nvme0n1p1";
			fsType = "vfat";
		};
	};

	swapDevices = [ { device = "/swapfile"; size = 10000; } ];
}
