{ pkgs, inputs, ... }:
pkgs.rustPlatform.buildRustPackage {
	pname = "tabs-kak-bin";
	version = "main";
	src = inputs.tabs-kak-src;
	cargoLock.lockFile = inputs.tabs-kak-src + "/Cargo.lock";
}
