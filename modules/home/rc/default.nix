{ config, lib, pkgs, ... }:
with lib; {
	options.programs.rc = with types; {
		enable = mkEnableOption "independent re-implementation for Unix of the Plan 9 shell";
		package = mkOption {
			type = types.package;
			default = pkgs.rc;
			defaultText = literalExpression "pkgs.rc";
			description = "The rc package to install. May be used to change the version or port.";
		};
		prompt = mkOption {
			description = "Prompts for the rc shell.";
			default = {};
			example = { static = false; values = [ "`{ pwd | sed s+$HOME+~+ }^' > '" "" ]; };
			type = submodule { options = {
				static = mkEnableOption "static shell prompts";
				values = mkOption {
					description = "The values used for the shell prompts.";
					default = [];
					example = [ "`{ pwd | sed s+$HOME+~+ }^' > '" "" ];
					type = listOf str;
				};
			};};
		};
		initFile = mkOption {
			description = "Location for the init file.";
			type = str;
			default = ".rcrc";
			example = "~/.config/rc/rcstart";
		};
		historyFile = mkOption {
			description = "Location for the history file.";
			type = nullOr str;
			default = null;
			example = ".local/state/rchist";
		};
		variables = mkOption {
			description = "An attribute set to be merged with environment.variables and added to config.";
			type = attrsOf str;
			default = {};
			example = { PAGER = "more"; };
		};
		aliases = mkOption {
			description = "An attribute set of rc functions with args provided.";
			type = attrsOf str;
			default = {};
			example = { "sduo" = "sudo"; };
		};
		functions = mkOption {
			description = "An attribute set of rc functions.";
			type = attrsOf str;
			default = {};
			example = { "killall" = "kill `{pidof $1}"; };
		};
		extra = mkOption {
			description = "More configs to add to the end of the rc config.";
			type = str;
			default = "";
			example = "sway";
		};
		shift-args = mkEnableOption "shifting the argument list on aliases";
	};
	config = mkIf config.programs.rc.enable {
		home.packages = [ config.programs.rc.package ];
		home.file.${config.programs.rc.initFile}.text = with config.programs.rc; ((
			concatLines (
				(mapAttrsToList (n: v: n+"='${if isList v then "( "+(concatStringsSep " " v)+" )" else v}'") variables) ++
				(mapAttrsToList (n: v: "fn ${n} { ${optionalString shift-args "shift; "} ${v} }") (functions//(builtins.mapAttrs (n: v: v+" $*") aliases)))
			)
		) + "\n" + (
			(optionalString (!prompt.static) "fn prompt ") + "{ prompt=(${concatStringsSep " " prompt.values}) }\n"
		) + (
			optionalString (historyFile != null) ("history="+historyFile))
		+"\n"+extra);
	};
}
