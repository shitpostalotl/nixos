{ osConfig, config, lib, ... }: {
	programs.carapace.enable = true;
	programs.zsh = {
		enable = true;
		dotDir = ".config/zsh";
		autocd = true;
		dirHashes = {
			x = "/etc/nixos";
			xh = "/etc/nixos/homes/x86_64-linux/axolotl";
			xs = "/etc/nixos/systems/x86_64-linux/global";
		};
		history = {
			append = true;
			ignoreDups = true;
			path = "${config.xdg.dataHome}/zsh/zsh_history";
		};
		localVariables.PROMPT = "%F{green}%~%f %F{cyan}% > %f";
		loginExtra = "test $(tty) = '/dev/tty2' && river; " + lib.optionalString (osConfig.networking.hostName != "floortop") "[ $(date +%H) -gt 15 ] && [ $(cat /sys/class/power_supply/BAT0/status) != 'Discharging' ] && bl 100 || bl 25";
		historySubstringSearch.enable = true;
		syntaxHighlighting.enable = true;
		sessionVariables = osConfig.environment.variables // config.wayland.windowManager.river.extraSessionVariables;
		shellAliases = osConfig.environment.shellAliases;
		initExtra = "bindkey '^[[1;5C' forward-word; bindkey '^[[1;5D' backward-word";
	};
}
