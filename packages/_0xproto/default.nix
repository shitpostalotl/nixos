{ pkgs, inputs, ... }:
pkgs.stdenv.mkDerivation {
	pname = "0xproto wo ligatures but w texture healing";
	version = "main";
	src = inputs._0xproto-src;
	nativeBuildInputs = with pkgs; [ gnumake (python3.withPackages (p: [p.appdirs p.attrs p.booleanoperations p.cffsubr p.compreffor p.fontmake p.fontmath p.fonttools p.fs p.glyphslib p.lxml p.openstep-plist p.pyclipper p.setuptools p.six p.ttfautohint-py p.ufo2ft p.ufolib2 p.unicodedata2])) ];
	patchPhase = ''
		ln -s ${pkgs.woff2}/bin woff2 # sending matype to code jail for Makefile crimes
		sed -i '/### programming ligatures/,/} dollar_greater;/d' sources/0xProto.glyphspackage/fontinfo.plist
		sed -i '/### programming ligatures/,/} dollar_greater;/d' sources/0xProto-Italic.glyphspackage/fontinfo.plist
	'';
	buildFlags = [ "build" ];
	installPhase = ''
		install -Dm644 -t $out/share/fonts/opentype/ fonts/*.otf
		install -Dm644 -t $out/share/fonts/truetype/ fonts/*.ttf
	'';
	
}
