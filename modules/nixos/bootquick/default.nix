{ config, lib, pkgs, ... }: with lib; {
	options = {
		boot.quick = mkEnableOption "tweaks to boot faster";
		networking.wired = mkEnableOption "wired networking instead of wireless";
	};

	config = lib.mkIf config.boot.quick {
		boot = {
			kernelParams = [ "console=tty1" ];
			initrd.systemd.enable = true;
			loader.timeout = 2;
		};
		systemd = {
			targets.network-online.wantedBy = mkForce [];
			services = {
				systemd-udev-settle.enable = false;
				iwd.after = [ "dbus.service" ];
				"getty@".serviceConfig.Type = mkForce "simple";
				systemd-user-sessions.serviceConfig = with pkgs; {
					ExecStartPost = kbd+"/bin/chvt 2";
					ExecStop = kbd+"/bin/chvt 1";
				};
			};
		};
		networking = {
			resolvconf.enable = true;
			dhcpcd = {
				enable = config.networking.wired;
				extraConfig = "noarp";
			};
			wireless.iwd = {
				enable = !(config.networking.wired);
				settings = {
					IPv6.Enabled = true;
					Settings.AutoConnect = true;
					General.EnableNetworkConfiguration = true;
					Network.NameResolvingService = "resolvconf";
				};
			};
		};
	};
}
