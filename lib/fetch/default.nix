{ inputs, lib }: let
	pkgs = inputs.nixpkgs.legacyPackages.x86_64-linux;
in {
	withCurlScript = { name, hash, script, impureEnvVars ? [], extraBuildInputs ? [] }: with pkgs; stdenv.mkDerivation {
		inherit name;
		builder = writeShellScript "builder.sh" "source $stdenv/setup; ${script}";
		nativeBuildInputs = [ (writeShellScriptBin "curl" ''${curl}/bin/curl --location --max-redirs 20 --retry 3 --disable-epsv --cookie-jar cookies --insecure --user-agent "curl/$(${curl}/bin/curl -V | head -1 | cut -d' ' -f2) Nixpkgs/${lib.trivial.release}" -s $*'') ] ++ extraBuildInputs;
		outputHashAlgo = "sha256";
		outputHash = hash;
		impureEnvVars = lib.fetchers.proxyImpureEnvVars ++ impureEnvVars;
	};

	fetchItchIO = key: { title, author, hash, platform ? "linux", printDebug ? false }: lib.fetch.withCurlScript {
		# Define a 2nd function that's just fetchItchIO with your ItchIO API key passed to it via secrets, use that function instead of this one
		inherit hash;
		name = title;
		extraBuildInputs = [pkgs.jq];
		script = let debugmsg = message: lib.optionalString printDebug message; in ''
			${debugmsg ">&2 echo -n gameid:"}
			gameid=$(curl https://${author}.itch.io/${title}/data.json | jq -cr '.id')
			${debugmsg ">&2 echo $gameid"}

			${debugmsg ">&2 echo -n uploadid:"}
			uploadid=$(curl https://itch.io/api/1/${key}/game/$gameid/uploads | jq '.uploads[] | select(.p_${platform}) |.id')
			test -z $uploadid && echo No version of ${title} exists for specified platform ${platform} && exit 2
			${debugmsg ">&2 echo $uploadid"}

			${debugmsg ">&2 echo -n url:"}
			url=$(curl https://itch.io/api/1/${key}/upload/$uploadid/download | jq -r '.url')
			${debugmsg ">&2 echo $url"}

			curl "$url" > $out
		'';
	};
}
