{ pkgs, lib, config, ... }: {
	imports = lib.snowfall.fs.get-non-default-nix-files ./.;
	boot = {
		kernelPackages = pkgs.linuxPackages_latest;
		kernelParams = [ "nowatchdog" ];
		loader = lib.mkDefault {
			systemd-boot = {
				enable = true;
				configurationLimit = 100;
				editor = false;
				extraEntries = {
					"she.conf" = "title She/Her\nefi /EFI/BOOT/BOOTX64.EFI\nsort-key ab_prn";
					"he.conf" = "title He/Him\nefi /EFI/BOOT/BOOTX64.EFI\nsort-key aa_prn";
					"they.conf" = "title They/Them\nefi /EFI/BOOT/BOOTX64.EFI\nsort-key ac_prn";
				};
			};
			efi.canTouchEfiVariables = true;
			efi.efiSysMountPoint = "/boot/efi";
		};
		binfmt.emulatedSystems = [ "aarch64-linux" ];
		quick = true;
	};

	hardware = {
		enableRedistributableFirmware = true;
		graphics.enable = true;
		bluetooth.enable = true;
	};
	hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
	powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
	nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";

	time.timeZone = "America/Los_Angeles";

	system = {
		stateVersion = lib.mkDefault "22.05";
		# nixos.tags = [ (self.shortRev or self.dirtyShortRev) ];
	};
}
