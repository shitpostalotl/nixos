{ pkgs, lib, config, host, osConfig, ... }: let
	invidious = "inv.nadeko.net";
	searx = "priv.au";
in {
	programs.qutebrowser = {
		enable = true;
		enableDefaultBindings = false;
		keyBindings = {
			normal = let
				passage = cmd: arg: with lib; with config.programs.password-store; pkgs.writeShellScript "qtbpassage" ((concatStrings (mapAttrsToList (k: v: "export ${k}=${v};") settings)) + ''echo fake-key $(passage ${arg} $(find $PASSAGE_DIR -type f | sed 's-'$PASSAGE_DIR'/--;s-\.age--' | tofi) | ${cmd}) > $QUTE_FIFO''); # TODO: autoselect
			in {
				# tabs
				"<ctrl-;>" = "cmd-set-text :";
				"<ctrl-t>" = "cmd-set-text -s :open -t";
				"<ctrl-o>" = "cmd-set-text -s :open ";
				"<ctrl-shift-t>" = "undo";
				"<alt-shift-t>" = "undo --window";
				"<alt-t>" = "tab-give";
				"<alt-o>" = "edit-url";
				"<ctrl-w>" = "tab-close";
				"<alt-w>" = "quit";
				"<ctrl-y>" = "yank";
				"<ctrl-r>" = "reload";
				"<alt-r>" = "reload -f";
				"<ctrl-tab>" = "tab-next";
				"<ctrl-shift-tab>" = "tab-prev";
				"<alt-tab>" = "back";
				"<alt-shift-tab>" = "forward";
				"<alt-p>" = "tab-pin";
				# sessions
				"<ctrl-s>l" = "session-load default";
				"<ctrl-s>w" = "session-save";
				"<ctrl-s>W" = "cmd-set-text -s :session-save";
				"<ctrl-s>d" = "cmd-set-text -s :session-delete";
				# view
				"<ctrl-0>" = "zoom 0";
				"<ctrl-->" = "zoom-out";
				"<ctrl-=>" = "zoom-in";
				"<ctrl-Up>" = "scroll-to-perc 0";
				"<ctrl-Down>" = "scroll-to-perc 100";
				# password
				"<ctrl-p>a" = "spawn --userscript ${passage "head -n2 | tac | sed 's/login: //' |  tr '\\n' '	' | sed 's/	/;;fake-key <tab>;;fake-key /'" ""}";
				"<ctrl-p>u" = "spawn --userscript ${passage "grep 'login: ' | sed 's/login: //'" ""}";
				"<ctrl-p>p" = "spawn --userscript ${passage "head -n1" ""}";
				"<ctrl-p>o" = "spawn --userscript ${passage "cat -" "otp"}";
				# settings
				"<alt-s>j" = "config-cycle content.javascript.enabled True False";
				"<alt-s>c" = "config-cycle content.canvas_reading True False";
				"<alt-s>w" = "config-cycle content.webgl True False";
				"<alt-s>p" = "config-cycle content.proxy 'system' 'socks://localhost:1080' 'http://localhost:4444'";
				"<alt-s>m" = "jmatrix-toggle";
				# searching
				"<alt-f>" = "cmd-set-text /";
				"<ctrl-[>" = "search-next";
				"<ctrl-]>" = "search-prev";
				# other stuff
				"<ctrl-m>" = "spawn ${pkgs.writeShellScript "mpv-inv" ''mpv $(echo $1 | sed 's-//[^/]*-//youtube.com-')''} {url}";
				"<alt-m>" = "tab-mute";
				"<ctrl-h>" = "open qute://help";
				"<Escape>" = "clear-keychain ;; search ;; fullscreen --leave";
				"<ctrl-e>" = "hint";
				"<ctrl-d>" = ''jseval [...document.querySelectorAll('[role="dialog"]')].map(e => e.remove())'';
			};
			command = {
				"<Return>" = "command-accept";
				"<Up>" = "completion-item-focus prev";
				"<Down>" = "completion-item-focus next";
				"<Escape>" = "mode-enter normal";
			};
			yesno = {
				"<Escape>" = "mode-leave";
				"<Return>" = "prompt-accept";
				"Y" = "prompt-accept --save yes";
				"N" = "prompt-accept --save no";
				"y" = "prompt-accept yes";
				"n" = "prompt-accept no";
			};
			prompt = {
				"<Escape>" = "mode-leave";
				"<Return>" = "prompt-accept";
				"<Backspace>" = "rl-filename-rubout";
				"<Tab>" = "prompt-open-download";
				"<ctrl-Tab>" = "prompt-open-download --pdfjs";
				"<ctrl-y>" = "prompt-yank";
				"<alt-y>" = "prompt-fileselect-external";
			};
			hint."<Escape>" = "mode-enter normal";
			caret."<Escape>" = "mode-enter normal";
			insert."<Escape>" = "mode-enter normal";
		};
		searchEngines = {
			DEFAULT = "https://${searx}/search?q={}";
			y = "https://${invidious}/search?q={}";
			p = "https://search.nixos.org/packages?channel=unstable&query={}";
			h = "https://home-manager-options.extranix.com/?query={}&release=master";
			w = "https://wolfreealpha.on.fleek.co/input?i={}&lang=en";
			m = "https://musicbrainz.org/search?query={}&type=artist&method=indexed";
			c = "https://search.worldcat.org/search?q={}";
			a = "https://annas-archive.se/search?q={}";
		};
		quickmarks = import secrets/bookmarks.nix;
		settings = {
			input = {
				insert_mode = {
					auto_enter = false;
					auto_leave = false;
					plugins = false;
				};
				forward_unbound_keys = "all";
				match_counts = false;
			};
			content = {
				headers = {
					user_agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36";
					referer = "never";
				};
				canvas_reading = false;
				webgl = false;
				webrtc_ip_handling_policy = "default-public-interface-only";
				cookies.accept = "no-3rdparty";
			};
			downloads = {
				location.directory = "/home/axolotl";
				remove_finished = 1000;
			};
			tabs.favicons.show = "pinned";
			url.default_page = "https://${searx}";
			url.start_pages = [ config.programs.qutebrowser.settings.url.default_page ];
			editor.command = [ "kitty" osConfig.environment.variables.EDITOR "{file}" ];
			content = {
				blocking.adblock.lists = [ "https://easylist.to/easylist/easylist.txt" "https://easylist.to/easylist/easyprivacy.txt" "https://raw.githubusercontent.com/liamengland1/miscfilters/master/yahoo.txt" "https://raw.githubusercontent.com/liamengland1/miscfilters/master/facebook.txt" "https://raw.githubusercontent.com/liamengland1/miscfilters/master/antipaywall.txt" "https://raw.githubusercontent.com/liamengland1/miscfilters/master/amazonannoyances.txt" "https://github.com/DandelionSprout/adfilt/raw/master/RickrollLinkIdentifier.txt" "https://github.com/DandelionSprout/adfilt/raw/master/LegitimateURLShortener.txt" "https://github.com/DandelionSprout/adfilt/raw/master/AntiFunctionalityRemovalList.txt" ];
				pdfjs = true;
				mute = true;
				autoplay = false;
			};
			qt = {
				chromium.low_end_device_mode = if host == "laptop" then "always" else "auto";
				workarounds = {
					remove_service_workers = true;
					disable_accelerated_2d_canvas = "never";
					disable_hangouts_extension = true;
				};
				highdpi = host == "floortop";
			};
			spellcheck.languages = [ "en-US" ];
			scrolling.bar = "when-searching";
		};
		extraConfig = ''
from qutebrowser.api import interceptor
from urllib.parse import urljoin
from urllib.request import urlopen
from PyQt6.QtCore import QUrl
from operator import methodcaller
from json import loads
import sys

sys.path.append("/home/axolotl/.config/qutebrowser/jmatrix")
config.source("jmatrix/jmatrix/integrations/qutebrowser.py")

config.set('content.cookies.accept', "all", '*://*.instructure.com/*')

def farside(url: QUrl, interceptor) -> bool:
	url.setHost('farside.link')
	url.setPath(urljoin(interceptor, url.path().strip('/')))

${lib.concatMapStrings (s: "def ${s}(url: QUrl) -> bool: return farside(url, '/${s}/')\n") [ "rimgo" "nitter" "scribe" "simplytranslate" "proxitok" "querte" "dumb" "anonymousoverflow" "quetre" "redlib" ]}

def f(info: interceptor.Request):
	if (info.resource_type != interceptor.ResourceType.main_frame or info.request_url.scheme() in {'data', 'blob'}): return
	url = info.request_url
	redir = {'youtu.be':methodcaller('setHost','${invidious}'),'youtube.com':methodcaller('setHost','${invidious}'),'reddit.com':redlib,'twitter.com':nitter,'mobile.twitter.com':nitter,'imgur.com':rimgo,'medium.com':scribe,'translate.google.com':simplytranslate,'vm.tiktok.com':proxitok,'tiktok.com':proxitok,'quora.com':quetre,'genius.com':dumb,'stackoverflow.com':anonymousoverflow,'npr.org':methodcaller('setHost','text.npr.org')}.get(url.host().replace("www.",""))
	if redir is not None and redir(url) is not False: info.redirect(url)
interceptor.register(f)
		'';
		greasemonkey = [
			({
				url = "https://gist.githubusercontent.com/beesarefriends/4a274256ef6c030b7c55fb4bbaf7ab35/raw/61ddb512021c2a3d701434cd9e6d29aefa4f5bde/dearrow-userscript.js";
				sha256 = "sha256-jNgmVXxMa+vGif1XL8e2gYqUgmbmuqjeMf8aMMw1DCQ=";
			} |> pkgs.fetchurl |> builtins.readFile |> builtins.replaceStrings ["yewtu.be"] [invidious] |> pkgs.writeText "dearrow.js")
		];
	};
	xdg.configFile = {
		"qutebrowser/jmatrix".source = pkgs.fetchgit {url="https://gitlab.com/jgkamat/jmatrix.git";hash="sha256-0owy4wIty6oDqPPYBen/qG6OcWBXLx3B+6HLtC/x2vg=";};
		"qutebrowser/jmatrix-rules".text = ''
https-strict: behind-the-scene false
matrix-off: behind-the-scene true
matrix-off: about-scheme true
matrix-off: chrome-extension-scheme true
matrix-off: chrome-scheme true
matrix-off: moz-extension-scheme true
matrix-off: opera-scheme true
matrix-off: vivaldi-scheme true
matrix-off: wyciwyg-scheme true
matrix-off: qute-scheme true
# [website] [1st-party/origin] [type] [allow/block]
* * * block
* * css allow
* * frame block
* * image allow
* 1st-party * allow
matrix-off: instructure.com true
matrix-off: nas.local true
accounts.google.com gstatic.com script allow
github.com github.githubassets.com script allow
khanacademy.org kastatic.org script allow
home-manager-options.extranix.com unpkg.com script allow
		'';
	};
	systemd.user.services.proxy = {
		Unit.Description = "various SSH port binds for my proxies";
		Service = {
			ExecStart = with pkgs; with unixtools; writeShellScript "ports" ''
				until ${ping}/bin/ping -c1 unix.dog && ${ping}/bin/ping -c1 axfedi.derg.rest; do sleep 1; done
				${openssh}/bin/ssh -ND 1080 shitpostalotl@unix.dog -v &
				${openssh}/bin/ssh -L 4444:0.0.0.0:4444 -L 7070:0.0.0.0:7070 axfedi.derg.rest -N -p 23 -v
			'';
			Restart = "on-failure";
		};
		Install.WantedBy = [ "default.target" ];
	};
}
