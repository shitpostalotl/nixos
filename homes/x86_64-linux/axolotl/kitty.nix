{ osConfig, ... }: {
	programs.kitty = {
		enable = true;
		keybindings = {
			# windows
			"ctrl+enter" = "launch --cwd=current";
			"ctrl+q" = "close_window";
			"ctrl+j" = "next_window";
			"ctrl+k" = "previous_window";
			"ctrl+shift+j" = "move_window_forward";
			"ctrl+shift+k" = "move_window_backward";
			"ctrl+x" = "toggle_layout stack";
			"f1" = "toggle_layout tall";
			# tabs
			"ctrl+shift+enter" = "new_tab";
			"ctrl+shift+q" = "close_tab";
			"ctrl+l" = "next_tab";
			"ctrl+h" = "previous_tab";
			"ctrl+shift+l" = "move_tab_forward";
			"ctrl+shift+h" = "move_tab_backward";
			# font
			"ctrl+shift+equal" = "change_font_size all +2.0";
			"ctrl+shift+plus" = "change_font_size all -2.0";
			"ctrl+equal" = "change_font_size all 0";
			# misc
			"ctrl+shift+c" = "copy_to_clipboard";
			"ctrl+shift+v" = "paste_from_clipboard";
			"ctrl+e" = "open_url_with_hints";
			"f2" = "show_scrollback";
			"shift+f2" = "show_last_command_output";
			"f3" = "clear_terminal scrollback active";
			"f4" = "load_config_file";
		};
		settings = {
			clear_all_shortcuts = "yes";
			cursor_shape = "beam";
			cursor_shape_unfocused = "unchanged";
			scrollback_fill_enlarged_window = "yes";
			wheel_scroll_min_lines = 10;
			strip_trailing_spaces = "smart";
			focus_follows_mouse = "yes";
			enable_audio_bell = "no";
			visual_bell_duration = 0.25;
			enabled_layouts = "fat,tall,stack";
			tab_bar_style  = "separator";
			tab_separator = " | ";
			tab_bar_align = "center";
			update_check_interval = 0;
			allow_remote_control = "yes";
			linux_display_server = "wayland";
			wayland_enable_ime = "no";
			select_by_word_characters = "-._~?&=%+#";
			symbol_map = "U+e000-U+e00a,U+ea60-U+ebeb,U+e0a0-U+e0c8,U+e0ca,U+e0cc-U+e0d7,U+e200-U+e2a9,U+e300-U+e3e3,U+e5fa-U+e6b1,U+e700-U+e7c5,U+ed00-U+efc1,U+f000-U+f2ff,U+f000-U+f2e0,U+f300-U+f372,U+f400-U+f533,U+f0001-U+f1af0 Symbols Nerd Font Mono";
		};
	};
	xdg.configFile."kitty/open-actions.conf".text = ''
protocol file
mime image/*
action launch --type=overlay kitten icat --hold -- ''${FILE_PATH}

protocol file
action launch ${osConfig.environment.variables.EDITOR} ''${FILE_PATH} +''${FRAGMENT}

protocol http, https
action launch ${osConfig.environment.variables.DEFAULT_BROWSER} ''${URL}
	'';
}
