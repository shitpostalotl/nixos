{ pkgs, inputs, ... }:
pkgs.stdenv.mkDerivation {
	pname = "neosurf";
	version = "main";
	src = inputs.neosurf-src;
	nativeBuildInputs = with pkgs; [ cmake python3Full gperf flex bison pkg-config gtk3 ]; # buildtime deps
	buildInputs = with pkgs; [ curl libressl libpsl libxml2 libjpeg libpng libwebp cairo pango wayland libxkbcommon ] ++ (with netsurf; [ nsgenbind libwapcaplet libutf8proc libsvgtiny libparserutils libnsutils libnspsl libnslog libnsgif libnsfb libnsbmp libhubbub libdom libcss ]); # runtime deps
}
