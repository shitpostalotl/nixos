# TODO: advanced outlining
# TODO: https://orgmode.org/features.html
{ pkgs, ... }: {
	programs.kakoune = {
		enable = true;
		config = {
			indentWidth = 0;
			tabStop = 4;
			showMatching = true;
			numberLines = {
				enable = true;
				highlightCursor = true;
			};
			scrollOff = {
				lines = 1;
				columns = 3;
			};
			ui.assistant = "cat"; # meow
			wrapLines = {
				enable = true;
				indent = true;
				word = true;
			};
			keyMappings = [
				{ mode = "insert"; key = "<c-w>"; docstring = "Delete word."; effect = "<a-;>b<a-;>d"; }
				{ mode = "user"; key = "e"; docstring = "Expand nested arguments enclosed by the parens under the cursor."; effect = "x:expand-parens<ret>"; }
				{ mode = "user"; key = "l"; effect = ":enter-user-mode lsp<ret>"; docstring = "Enter LSP mode."; }
				{ mode = "user"; key = "s"; effect = ":spell<ret>"; docstring = "Spellcheck the buffer."; }
				{ mode = "user"; key = "g"; effect = ":grep "; docstring = "Find some text."; }
			];
			hooks = [
				{
					group = "global";
					name = "InsertCompletionShow";
					option = ".*";
					commands = ''
try %{
	execute-keys -draft 'h<a-K>\h<ret>'
	map window insert <tab> <c-n>
	map window insert <s-tab> <c-p>
	hook -once -always window InsertCompletionHide .* %{
		unmap window insert <tab> <c-n>
		unmap window insert <s-tab> <c-p>
	}
}
					'';
				}
				{
					group = "global";
					name = "WinSetOption";
					option = "filetype=scheme";
					commands = ''
remove-hooks window scheme-indent
parinfer-enable-window -smart
					'';
				}
				{
					group = "global";
					name = "WinCreate";
					option = ".*";
					commands = "kakboard-enable";
				}
			] ++ (builtins.map (x: { group = "global"; name = "WinCreate"; option = ".*\\."+x.extention; commands = "set window filetype ${x.extention}\nset window lsp_language_id %opt{filetype}\n${x.extra}"; }) [
				{ extention = "lobster"; extra = ""; }
			]);
		};
		plugins = with pkgs.kakounePlugins; [ tabs-kak auto-pairs-kak kakboard kakoune-easymotion pkgs.xltl.kak-luar parinfer-rust ];
		extraConfig = ''
tabs-recommended-keys
set-option global tabs_modelinefmt '%val{cursor_line}:%val{cursor_char_column} {{mode_info}} '
set-option global tabs_options --minified

eval %sh{kak-lsp}
set global lsp_debug true
lsp-enable
#set-option global lsp_auto_highlight_references true
#lsp-inlay-hints-enable global
#lsp-auto-hover-enable

enable-auto-pairs
require-module luar

set-option global grepcmd '${pkgs.ripgrep}/bin/rg --trim --vimgrep'

define-command expand-parens -hidden %{
	lua %val{selection} %{
		state = args()
		Indentation = string.len(string.match(state, "^(%s*)"))
		return state:gsub(".", function(c)
			if c == "(" then
				Indentation = Indentation + 1
				return "(\n"..string.rep("\t", Indentation)
			elseif c == ")" then
				Indentation = Indentation - 1
				return "\n"..string.rep("\t", Indentation)..")"
			elseif c == "," then
				return ",\n"..string.rep("\t", Indentation)
			end
		end)
	}
}

def popup -params 1 -docstring 'popup <cli command>: run a kitty overlay window with the thing.' %{ evaluate-commands %sh{ kitten @ launch --no-response --type overlay-main --copy-env --cwd current $1 } }
		'';
	};
	home.packages = with pkgs; [
		wl-clipboard
		(aspellWithDicts (d: [d.en d.en-computers]))
		kakoune-lsp
		xltl.tabs-kak-rs
		lua5_4_compat
	];
	xdg.configFile."kak-lsp/kak-lsp.toml".source = (pkgs.formats.toml {}).generate "kak-lsp.toml" {	language = {
		nix = {
			filetypes = [ "nix" ];
			roots = [ "flake.nix" ];
			command = pkgs.nil+"/bin/nil";
		};
		python = {
			filetypes = [ "python" ];
			roots = [ "requirements.txt" "setup.py" "pyproject.toml" ".git" "flake.nix" ];
			command = pkgs.ruff+"/bin/ruff";
			args = [ "server" "--preview" ];
		};
		javascript = {
			roots = [ ];
			filetypes = [ "javascript" "typescript" ];
			command = pkgs.typescript-language-server+"/bin/typescript-language-server";
			args = [ "--stdio" ];
		};
		lua = {
			roots = [ ];
			filetypes = [ "lua" ];
			command = pkgs.lua-language-server+"/bin/lua-language-server";
		};
		scheme = {
			roots = [ ];
			filetypes = [ "scheme" ];
			command = pkgs.akkuPackages.scheme-langserver+"/bin/scheme-langserver";
		};
		lobster = {
			roots = [ ];
			filetypes = [ "lobster" ];
			command = pkgs.xltl.lobster-lsp+"/bin/lobster-lsp";
			settings_section = "lobster";
			settings.lobster.executable = pkgs.lobster+"/bin/lobster";
			args = [ "--stdio" ];
		};
	}; };
}
