{ config, lib, pkgs, ... }: {
	options.networking.nw-hosts = with lib; mkOption {
		type = with types; nullOr (attrsOf (attrsOf (listOf str)));
		default = null;
		description = "Locally defined maps of hostnames to IP addresses depending on the network name. Note that this enables statefull /etc/hosts and manages it statefully. Sucks but this is the only way to do it afaik.";
		example = ''"homenet" = { "10.0.0.85" = [ "serv.local" ]; }; "*" = { "198.51.100.0" = [ "serv.local" ]; };'';
	};

	config = with lib; mkIf (builtins.isAttrs config.networking.nw-hosts) {
		systemd.services.nw-dep-hosts = {
			description = "Change /etc/hosts based on network.";
			after = [ "network.target" "iwd.target" "dhcpcd.service" ];
			wantedBy = [ "multi-user.target" "sysinit-reactivation.target" ];
			before = [ "network-online.target" ];
			serviceConfig = {
				Type = "oneshot";
				ExecStart = with pkgs; with lib; pkgs.writeShellScript "nw-dep-hosts.sh" ''
					sed -n '/---NETWORK-DEPENDANT---/q;p' -i /etc/hosts
					echo "#---NETWORK-DEPENDANT---" >> /etc/hosts
					while [ "$nw" = "" ] && [ "$e" = "" ]; do
						nw=$(${wirelesstools}/bin/iwgetid -r)
						e=$(${ethtool}/bin/ethtool $(${busybox}/bin/ip -o -4 route show to default | ${gawk}/bin/awk '{print $5}') 2> /dev/null | grep 'Link detected: yes')
						sleep 0.1
					done
					echo "# $nw" >> /etc/hosts
					case $nw in
						${concatLines (reverseList (mapAttrsToList (nw: h: nw+'') echo "''+concatLines (flatten (mapAttrsToList (n: v: map (x: n+" "+x) v) h))+''" >> /etc/hosts;;'') config.networking.nw-hosts))}
					esac
				'';
			};
		};
		environment.etc.hosts.mode = "0644";
	};
}
