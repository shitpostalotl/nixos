{ pkgs, lib, host, ... }: {
	services.mako = { enable = true; maxIconSize = 0; };
	programs = {
		tofi = {
			enable = true;
			package = pkgs.tofi.overrideAttrs { patches = [ (pkgs.writeText "fix.diff" ''
diff --git a/src/main.c b/src/main.c
index e91838f..b2cd429 100644
--- a/src/main.c
+++ b/src/main.c
@@ -1576,7 +1576,7 @@ int main(int argc, char *argv[])
 	tofi.window.zwlr_layer_surface = zwlr_layer_shell_v1_get_layer_surface(
 			tofi.zwlr_layer_shell,
 			tofi.window.surface.wl_surface,
-			wl_output,
+			nullptr,
 			ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY,
 			"launcher");
 	zwlr_layer_surface_v1_set_keyboard_interactivity(
			'') ]; };
			settings = {
				late-keyboard-init = true;
				auto-accept-single = true;
				hint-font = false;
				ascii-input = true;
				border-width = 0;
				outline-width = 1;
				prompt-text = "";
				height = "50%";
				width = "50%";
			};
		};
	};
	home.packages = with pkgs; with lib; let
		mons = {
			"floortop" = {
				"HDMI-A-1" = { pos = "1080,0"; preferred = true; };
				"DP-2" = { transform = "270"; pos = "0,0"; };
			};
			"laptop" = {
				"HDMI-A-1" = { pos = "0,0"; preferred = true; };
				"eDP-1".pos = "1920,120";
			};
		};
	in with lib; [
		grim
		(writeShellScriptBin "monitors" "${wlr-randr}/bin/wlr-randr ${concatStringsSep " " (flatten (mapAttrsToList (m: s: (["--output ${m}"]++(mapAttrsToList (f: v: "--"+f+(optionalString (isString v) (" "+v))) s))) (optionalAttrs (hasAttr host mons) mons.${host})))}")
	];
}
