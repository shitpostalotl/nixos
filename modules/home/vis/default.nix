{ config, lib, pkgs, inputs, ... }: with lib; {
	options.programs.vis = with types; {
		enable = mkEnableOption "a vim like editor";
		package = mkOption {
			type = package;
			default = pkgs.vis;
			defaultText = literalExpression "pkgs.vis";
			description = "The vis package to install. Will be modified to wrap pluginDeps.";
		};
		plugins = mkOption {
			description = "Plugins to load via vis-plug";
			default = [];
			type = listOf (either str (submodule { options = let
				optional-vis-plug = desc: mkOption { description = desc; default = null; type = nullOr str; };
			in {
				url = mkOption {
					description = "the url to the git repo (https://github.com or https:// can be omitted)";
					default = "";
					type = str;
				};
				file = optional-vis-plug "the relative path to the lua file (defaults to init, skip the .lua part)";
				ref = optional-vis-plug "checkout a spesific commit, branch or tag";
				alias = optional-vis-plug "access plugins via plug.plugins.{alias}";
				theme = optional-vis-plug "set true if theme; will set theme on INIT event";
			};}));
			example = [ "codeberg.org/shitpostalotl/vis-bpts" { url = "gitlab.com/muhq/vis-lspc"; alias = "vislspc"; ref = "lua52-comp"; } ];
		};
		pluginDeps = mkOption {
			description = "List of packages to install in the vis PATH, mainly for plugins.";
			default = [];
			type = listOf package;
			example = [ pkgs.python311Packages.pyflakes pkgs.python311Packages.pycodestyle ];
		};
		options = {
			vis = mkOption {
				description = "The global settings for vis";
				type = attrs;
				default = {};
				example = { ai = true; };
			};
			win = mkOption {
				description = "The window-specific settings for vis";
				type = attrs;
				default = {};
				example = { nu = true; showeof = false; };
			};
		};
		keymaps = mkOption {
			description = "Key bindings for vis";
			default = {};
			type = attrsOf (attrsOf str);
			example = { NORMAL = { "U" = "<vis-redo>"; }; INSERT = { "<C-;>" = "<Escape>:"; }; };
		};
		lexers = mkOption {
			description = "New lexers to install.";
			default = {};
			example = { mail = { source = pkgs.vis+"/share/vis/lexers/text.lua"; fileExtensions = [ "eml" ]; }; };
			type = attrsOf (submodule { options = {
				source = mkOption {
					description = "Path to a file with the lua code of the lexer.";
					default = null;
					example = ./lexers/nix.lua;
					type = path;
				};
				fileExtensions = mkOption {
					description = "file extensions to associate the lexer with.";
					default = [];
					example = [ "nix" ];
					type = listOf str;
				};
			}; });
		};
		extraConfig = mkOption {
			type = str;
			description = "The options to use for configuring vis in visrc.lua.";
			default = "";
		};
	};
	config = with lib; let cfg = config.programs.vis; in mkIf cfg.enable {
		home.packages = [ (cfg.package.overrideAttrs (oldAttrs: {postInstall=(removeSuffix "\n" oldAttrs.postInstall)+" --prefix PATH ':' \"\$PATH:${makeBinPath config.programs.vis.pluginDeps}\"";})) ];
		xdg.configFile = {
			"vis/visrc.lua".text = let
				pluginsStr = concatStringsSep ", " (map (i: if (builtins.isAttrs i) then "{ "+(concatStrings (mapAttrsToList (n: v: optionalString (v!=null) "${n} = '${v}', ") i))+" }" else ''{ '${i}' }'') cfg.plugins);
				optionsStr = ns: concatStringsSep "; " (mapAttrsToList (n: v: ns+".options."+n+" = "+(if (builtins.isBool v) then (if v then "true" else "false") else builtins.toString v)) cfg.options.${ns});
			in ''
require('vis')
${optionalString (cfg.plugins != []) ''plug = require('plugins/vis-plug').init({${pluginsStr}})''}
${optionsStr "vis"}
vis.events.subscribe(vis.events.WIN_OPEN, function(win) ${optionsStr "win"} end)
${concatLines (flatten (mapAttrsToList (mode: val: mapAttrsToList (key: act: "vis:map(vis.modes.${mode}, '${key}', '${act}')") val) cfg.keymaps))}
${concatLines (mapAttrsToList (n: v: "vis.ftdetect.filetypes.${n} = { ext = { ${ concatMapStringsSep ", " (x: "'%.${x}$'") v.fileExtensions } } }") cfg.lexers)}
${cfg.extraConfig}
			'';
		} // mapAttrs' (n: v: nameValuePair ("vis/lexers/${n}.lua") {source = v.source;}) cfg.lexers // optionalAttrs (cfg.plugins != []) {"vis/plugins/vis-plug.lua".source = inputs.vis-plug+"/init.lua";};
	};
}
