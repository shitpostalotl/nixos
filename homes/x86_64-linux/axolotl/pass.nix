{ pkgs, ... }: {
	programs.password-store = {
		enable = true;
		package = pkgs.passage;
		settings = {
			PASSAGE_AGE = "rage";
			PASSAGE_DIR = "/home/axolotl/Documents/pass";
			PASSAGE_IDENTITIES_FILE = "/mnt/sd/key.age";
			PASSWORD_STORE_ENABLE_EXTENSIONS = "true";
			PASSAGE_EXTENSIONS_DIR = "/home/axolotl/.local/share/passage-extensions";
			PASSAGE_RECIPIENTS = "age1677pgh5jdd8mvpm866utge7sjdtvqy7eaa8y8lgv8x05v7rf7dyslclfaj";
		};
	};
	home.packages = with pkgs; [ rage ];
	xdg.dataFile."passage-extensions/otp.bash".source = (pkgs.passExtensions.pass-otp.overrideAttrs { src = pkgs.fetchFromGitHub { owner = "remko"; repo = "pass-otp"; rev = "develop"; sha256 = "sha256-YyW4kt7O34K5vZt9U6R7VS9wag767uL2EDkIL5Fzhhc="; }; }) + "/lib/password-store/extensions/otp.bash";
}
