{ config, lib, pkgs, ... }:
with lib; {
	options.programs.uxn = with types; {
		enable = mkEnableOption "uxn virtual machene";
		enableLauncher = mkEnableOption "uxn rom launcher";
		package = mkOption {
			type = types.package;
			default = pkgs.uxn;
			defaultText = literalExpression "pkgs.uxn";
			description = "The uxn package to install.";
		};
		roms = mkOption {
			description = "Roms for the uxn virtual machene.";
			default = [];
			type = listOf package;
		};
		dir = mkOption {
			description = "Directory for the roms for the uxn virtual machene.";
			default = config.xdg.dataHome + "/uxn";
			type = str;
		};
	};
	config = let
		cfg = config.programs.uxn;
	in mkIf cfg.enable {
		home.packages = [
			cfg.package
		] ++ (optional cfg.enableLauncher (pkgs.writeShellScriptBin
			"uxn"
			''[ $# -eq 1 ] && uxnemu ${cfg.dir}/$1 || find ${cfg.dir}/ -name "*.rom" | sed "s-${cfg.dir}/--"''
		));
		home.file.".local/share/uxn".source = pkgs.stdenv.mkDerivation {
			name = "uxn-roms";
			dontUnpack = true;
			installPhase = "mkdir -p $out;" + (lib.concatMapStrings (r: "mkdir $out/${r.name}; cp -r ${r.outPath}/* $out/${r.name}; ") cfg.roms);
		};
	};
}
