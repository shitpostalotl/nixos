{ ... }: {
	programs.eza = {
		enable = true;
		enableZshIntegration = true;
		git = true;
		colors = "always";
		icons = "never";
		extraOptions = [
			"--long"
			"--group"
			"--header"
			"--modified"
			"--extended"
			"--group-directories-first"
		];
	};
}
